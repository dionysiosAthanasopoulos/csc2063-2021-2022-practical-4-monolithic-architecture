package Controller;

import DBStorage.DBStorage;
import FileStorage.FileStorage;


public class StorageController {

	public void storage( StorageType storageType ) {

		if( storageType == StorageType.FILE ) { FileStorage fileStorage = new FileStorage(); }

		else if( storageType == StorageType.DB ) { DBStorage dbStorage = new DBStorage(); }
	}
}